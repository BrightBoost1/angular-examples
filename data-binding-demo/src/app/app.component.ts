import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title: string = 'Courses';
  courses: Array<any> = [];

  // Properties that define data for a course
  // propName: type = defaultValue
  courseName: string = '';
  courseNum: string = '';
  startDate: string = '';
  department: string = '';

  // A properties that helps us know if the user added a
  // new course
  addNewCourses: boolean = false;

  // A miscellaneous property for copyright and a method that
  // can be used to access it
  currentYear: number = 2023;

  // declare a method
  getCurrentYear(): number {
    return this.currentYear;
  }

  addNewCourse() {
    let course = {
      courseName: this.courseName,
      courseNum: this.courseNum,
      startDate: this.startDate,
      department: this.department,
    };
    this.cleanForm();
    this.courses.push(course);
  }

  cleanForm() {
    this.courseName = '';
    this.courseNum = '';
    this.startDate = '';
    this.department = '';
  }
}
