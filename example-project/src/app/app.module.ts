import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ShowPersonComponent } from './show-person/show-person.component';
import { ItemListComponent } from './item-list/item-list.component';
import { UpdateExerciseComponent } from './update-exercise/update-exercise.component';
import { TestComponent } from './header/test/test.component';
import { HomeComponent } from './home/home.component';
import { DifferentComponent } from './different/different.component';
import { DifferentNewComponent } from './different-new/different-new.component';
import { DataBindingExamplesComponent } from './data-binding-examples/data-binding-examples.component';
import { PropertyBindingComponent } from './property-binding/property-binding.component';
import { TwoWayBindingExampleComponent } from './two-way-binding-example/two-way-binding-example.component';
import { EventBindingExampleComponent } from './event-binding-example/event-binding-example.component';
import { InteractionComponent } from './interaction/interaction.component';
import { InteractionInnerComponent } from './interaction-inner/interaction-inner.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivesExamplesComponent } from './directives-examples/directives-examples.component';
import { SayingsComponent } from './sayings/sayings.component';
import { LibraryComponent } from './library/library.component';
import { FormExerciseComponent } from './form-exercise/form-exercise.component';
import { CallingApisComponent } from './calling-apis/calling-apis.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'noteboard/sub/domain', component: HomeComponent },
  { path: 'unrelated', component: TestComponent },
  { path: 'different', component: DifferentComponent },
  { path: 'different-new', component: DifferentNewComponent },
  { path: 'data', component: DataBindingExamplesComponent },
  { path: 'property', component: PropertyBindingComponent },
  { path: 'two', component: TwoWayBindingExampleComponent },
  { path: 'event', component: EventBindingExampleComponent },
  { path: 'interaction', component: InteractionComponent },
  { path: 'directive', component: DirectivesExamplesComponent },
  { path: 'sayings', component: SayingsComponent },
  { path: 'books', component: LibraryComponent },
  { path: 'contact', component: FormExerciseComponent },
  { path: 'calling', component: CallingApisComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShowPersonComponent,
    ItemListComponent,
    UpdateExerciseComponent,
    TestComponent,
    HomeComponent,
    DifferentComponent,
    DifferentNewComponent,
    DataBindingExamplesComponent,
    PropertyBindingComponent,
    TwoWayBindingExampleComponent,
    EventBindingExampleComponent,
    InteractionComponent,
    InteractionInnerComponent,
    FooterComponent,
    DirectivesExamplesComponent,
    SayingsComponent,
    LibraryComponent,
    FormExerciseComponent,
    CallingApisComponent,
  ],
  imports: [BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes)],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
