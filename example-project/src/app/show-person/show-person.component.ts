import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-show-person',
  templateUrl: './show-person.component.html',
  styleUrls: ['./show-person.component.css'],
})
export class ShowPersonComponent implements OnChanges, OnInit {
  @Input() name!: string;
  @Input() age!: number;

  ngOnChanges(changes: SimpleChanges) {
    console.log('The values changed: ', changes);
    console.log('The name used to be: ', changes['name'].previousValue);
  }

  ngOnInit(): void {
    console.log('On init function!');
    this.name = 'Mark';
    this.age = 37;
  }
}
