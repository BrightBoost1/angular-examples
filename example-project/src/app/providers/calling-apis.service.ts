import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DogImage } from '../models/dog-image.model';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root',
})
export class CallingApisService {
  constructor(private http: HttpClient) {}
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getTodo(id: number): Observable<Todo> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/todos/' + id, this.httpOptions)
      .pipe(map((res) => <Todo>res));
  }

  getDog(): Observable<DogImage> {
    return this.http
      .get('https://dog.ceo/api/breeds/image/random', this.httpOptions)
      .pipe(map((res) => <DogImage>res));
  }
}
