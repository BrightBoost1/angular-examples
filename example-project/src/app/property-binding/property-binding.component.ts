import { Component } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.css']
})
export class PropertyBindingComponent {
  url: string = "assets/puppy.jpg";
  layout: string = "red";
  user: any = {
    username: "Rose",
    age: 57,
    avatar: "assets/rose.jpg"
  }
  color: string = "yes";

  toggleStuff() {
    if(this.url == "assets/puppy.jpg") {
      this.url = "assets/puppies.jpg";
    } else {
      this.url = "assets/puppy.jpg";
    }

    if(this.layout == "red") {
      this.layout = "green";
    } else {
      this.layout = "red";
    }
  }

}
