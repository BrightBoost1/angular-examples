import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() headerName: string = "";
  @Output() textForFooterEvent = new EventEmitter<string>();

  
  ngOnInit() {
    this.textForFooterEvent.emit("some text for footer");
  }

}
