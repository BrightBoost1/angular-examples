import { Component } from '@angular/core';

@Component({
  selector: 'app-two-way-binding-example',
  templateUrl: './two-way-binding-example.component.html',
  styleUrls: ['./two-way-binding-example.component.css']
})
export class TwoWayBindingExampleComponent {
  firstName: string = "";
  residesIn: string = "";
}
