import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DifferentNewComponent } from './different-new.component';

describe('DifferentNewComponent', () => {
  let component: DifferentNewComponent;
  let fixture: ComponentFixture<DifferentNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DifferentNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DifferentNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
