import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SayingsComponent } from './sayings.component';

describe('SayingsComponent', () => {
  let component: SayingsComponent;
  let fixture: ComponentFixture<SayingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SayingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SayingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
