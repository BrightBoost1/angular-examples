import { Component } from '@angular/core';

@Component({
  selector: 'app-sayings',
  templateUrl: './sayings.component.html',
  styleUrls: ['./sayings.component.css'],
})
export class SayingsComponent {
  title = 'Sayings';
  // raw data
  categories: Array<string> = [];
  sayings: Array<any> = [];
  // sayings that match a selection
  matchingSayings: Array<any> = [];
  constructor() {
    // load the sayings categories
    this.categories = ['Inspiration', 'Staying Safe', 'Statistics'];
    // load the sayings
    this.sayings = [
      {
        category: 'Inspiration',
        quote:
          "You're braver than you believe, and stronger than you seem, and smarter than you think.",
      },
      {
        category: 'Inspiration',
        quote:
          'Nothing is particularly hard if you break it down into small jobs.',
      },
      {
        category: 'Staying Safe',
        quote: 'An apple a day keeps the doctor away.',
      },
      {
        category: 'Staying Safe',
        quote:
          'A ship in a harbor is safe, but that is not what ships are for.',
      },
      {
        category: 'Statistics',
        quote:
          'He uses statistics as a drunken man uses lamp posts... for support rather than illumination.',
      },
    ];
    // set up for when page loads and no sayings show
    this.matchingSayings = [];
  }
  onSelectSaying(event: any): void {
    // the value from the DDL is available via the event
    // parameter
    const selectedCategory = event.target.value;
    //console.log(
    // 'onSelectSaying called... selected value is...'
    // + selectedCategory)
    // if they pick the select one option...
    if (selectedCategory == '') {
      this.matchingSayings = [];
    }
    // otherwise... find the matching sayings
    this.matchingSayings = this.sayings.filter(
      (s) => s.category == selectedCategory
    );
  }
}
