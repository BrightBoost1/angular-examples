import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-update-exercise',
  templateUrl: './update-exercise.component.html',
  styleUrls: ['./update-exercise.component.css'],
})
export class UpdateExerciseComponent implements AfterViewInit {
  @ViewChild('message') messageElement!: ElementRef;

  constructor() {}

  ngAfterViewInit(): void {
    this.messageElement.nativeElement.innerText = 'Hello, something!';
  }
}
