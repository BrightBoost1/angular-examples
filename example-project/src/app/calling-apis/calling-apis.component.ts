import { Component, OnInit } from '@angular/core';
import { DogImage } from '../models/dog-image.model';
import { Todo } from '../models/todo.model';
import { CallingApisService } from '../providers/calling-apis.service';

@Component({
  selector: 'app-calling-apis',
  templateUrl: './calling-apis.component.html',
  styleUrls: ['./calling-apis.component.css'],
})
export class CallingApisComponent implements OnInit {
  dogImage: DogImage = new DogImage("", "");
  todo: Todo = new Todo(1, 1, "", false);
  constructor(private callingApisService: CallingApisService) {}

  ngOnInit(): void {
    this.callingApisService.getTodo(1).subscribe((data) => {
      console.dir(data);
      this.todo = data;
    });
  }
}
