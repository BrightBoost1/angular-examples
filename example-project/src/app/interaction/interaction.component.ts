import { Component } from '@angular/core';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.css'],
})
export class InteractionComponent {
  someVariable: string = '';
  greeting: string = "Hello";
  processMessage($event: any) {
    console.log('Received: ' + $event);
    this.someVariable = $event;
  }

  saveMessage($event: any) {
    console.log('Saved: ' + $event);
    this.someVariable = $event;
  }
}
